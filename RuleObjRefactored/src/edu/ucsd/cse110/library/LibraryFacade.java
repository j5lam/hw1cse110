package edu.ucsd.cse110.library;

import java.time.LocalDate;

import edu.ucsd.cse110.library.rules.*;


public class LibraryFacade {
	public void checkoutPublication(Member mem, Publication pub, LocalDate date){
		pub.checkout(mem,date);
	}
	
    public void checkoutPublication(Member mem, Publication pub){
    	LocalDate checkoutDate = LocalDate.now();
    	checkoutPublication(mem,pub,checkoutDate);
    }
    public void returnPublication(Publication pub, LocalDate date){
    	pub.pubReturn(date);
    }
    public void returnPublication(Publication pub){
    	LocalDate returnDate = LocalDate.now();
    	returnPublication(pub, returnDate);
    	
    }
    public double getFee(Member mem){
    	return mem.getDueFees();
    }
    public boolean hasFee(Member mem){
    	return ( mem.getDueFees() != 0) ? true: false;
    }
    
}
